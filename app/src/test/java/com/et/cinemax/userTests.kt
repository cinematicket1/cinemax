package com.et.cinemax

import android.app.Application
import androidx.room.Room
import com.et.cinemax.data.CineDatabase
import com.et.cinemax.data.Movie
import com.et.cinemax.data.User
import com.et.cinemax.viewModel.MovieViewModel
import com.et.cinemax.viewModel.UserViewModel
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class userTests {
    private lateinit var Cdatabase: CineDatabase
    lateinit var  userVM : UserViewModel
    lateinit var  movieVM : MovieViewModel
    @Before
    fun setUpContext(){
        //val appcontext =""
        //Cdatabase = Room.inMemoryDatabaseBuilder(appcontext,CineDatabase::class.java).build()
        userVM= UserViewModel(Application())
        movieVM= MovieViewModel(Application())
    }

    @Test
    fun TestFetchandSaveandDeleteUser()=runBlocking {
        val TestUser = User(1,0,"test","test@test.com","123456789","xx",0)
        userVM.saver(TestUser)
        var result=userVM.user

        Assert.assertThat(result, CoreMatchers.notNullValue())

        userVM.userRepo.DeleteAll()

        result=userVM.user

        Assert.assertThat(result, CoreMatchers.notNullValue())


    }
    @Test
    fun AddMpvieTest(){
        val movie= Movie(6,"","","","","","","","","","")
        movieVM.Save(movie)
        var res=movieVM.allMovies
        Assert.assertThat(res, CoreMatchers.notNullValue())


    }
}