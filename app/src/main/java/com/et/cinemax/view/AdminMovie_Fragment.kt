package com.et.cinemax.view

import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.et.cinemax.R
import com.et.cinemax.data.CineDatabase
import com.et.cinemax.data.Movie
import com.et.cinemax.Repo.MovieRepo
import com.et.cinemax.databinding.FragmentAdminmovieBinding
import com.et.cinemax.viewModel.MovieViewModel

class AdminMovie_Fragment : Fragment() {

    lateinit var movieViewModeladmin: MovieViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var adminMovieBinding=
            DataBindingUtil.inflate<FragmentAdminmovieBinding>(inflater,
                R.layout.fragment_adminmovie,container, false)

        val view= inflater.inflate(R.layout.fragment_adminmovie, container, false)
        movieViewModeladmin = ViewModelProviders.of(activity!!).get(MovieViewModel::class.java)
        adminMovieBinding.adminMovieAdd=movieViewModeladmin
        adminMovieBinding.AddBalanceButton.setOnClickListener {
            val movTit=adminMovieBinding.adminMovieAdd!!.title.value
            val movGen=adminMovieBinding.adminMovieAdd!!.genere.value
            val movAmo=adminMovieBinding.adminMovieAdd!!.amount.value
            val movLan=adminMovieBinding.adminMovieAdd!!.language.value
            val movTime=adminMovieBinding.adminMovieAdd!!.time.value
            val movDate=adminMovieBinding.adminMovieAdd!!.date.value
            val movImg=adminMovieBinding.adminMovieAdd!!.imglink.value
            val movDesc=adminMovieBinding.adminMovieAdd!!.description.value
            val NewMov: Movie = Movie(0,
                movTit.toString(),
                movGen.toString(),
                movLan.toString(),
                movAmo.toString(),
                movTime.toString(),
                movDate.toString(),
                "",
                "1hr:30min",
                movImg.toString(),
                movDesc.toString())
            AsyncTask.execute{
                val movieDao= CineDatabase.getDatabase(activity!!).movieDao()
                val movieRepo= MovieRepo(movieDao)
                try {
                    val movieAddResp=movieRepo.SaveToNetwork(NewMov).execute().body()
                    activity!!.runOnUiThread {
                        if (movieAddResp.equals("1")){
                            Toast.makeText(activity,"SuccessFully Added New Movie",
                                Toast.LENGTH_LONG).show()

                        }
                        else{
                            Toast.makeText(activity,"Error While Adding Movie",
                                Toast.LENGTH_LONG).show()
                        }
                    }
                }
                catch (ex : Exception){
                    ex.printStackTrace()
                    activity!!.runOnUiThread {
                        Toast.makeText(activity,"Connection Timeout",
                            Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
        return adminMovieBinding.root
    }


}
