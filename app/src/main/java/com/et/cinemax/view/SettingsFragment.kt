package com.et.cinemax.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.et.cinemax.R
import com.et.cinemax.databinding.FragmentSettingsBinding
import com.et.cinemax.viewModel.UserViewModel


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class SettingsFragment : Fragment() {

    lateinit var userViewModelsetting: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var SettingsBinding=DataBindingUtil.inflate<FragmentSettingsBinding>(inflater,
            R.layout.fragment_settings,container, false)
        userViewModelsetting = ViewModelProviders.of(activity!!).get(UserViewModel::class.java)
        userViewModelsetting.getUserInfo()
        userViewModelsetting.user.observe(this, Observer { user ->
            user?.let {
                SettingsBinding.userModelSetting=user
            }

        })
        return SettingsBinding.root
    }


}
