package com.et.cinemax.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.et.cinemax.MainActivity
import com.et.cinemax.R
import com.et.cinemax.viewModel.UserViewModel
import com.et.cinemax.databinding.FragmentLoginBinding
import com.google.android.material.snackbar.Snackbar


class LoginFragment : Fragment() {
    lateinit var userViewModelTest:UserViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //val view = inflater.inflate(R.layout.fragment_login, container, false)
        var LoginBinding=DataBindingUtil.inflate<FragmentLoginBinding>(inflater,
            R.layout.fragment_login,container, false)
        userViewModelTest = ViewModelProviders.of(activity!!).get(UserViewModel::class.java)
        LoginBinding.userFromView=userViewModelTest

        LoginBinding.LoginButton.setOnClickListener {
            //Toast.makeText(activity,"Data Binding - ${LoginBinding.userFromView!!.userPhone.value} + pass= ${LoginBinding.userFromView!!.userPass.value}",Toast.LENGTH_LONG).show()
            val uphone=LoginBinding.userFromView!!.userPhone.value
            val upassword=LoginBinding.userFromView!!.userPass.value
            if (uphone!=null && upassword!=null){
                (activity as MainActivity).FinalLogin(uphone,upassword)
            }
            else{
                Snackbar.make(LoginBinding.root,
                    "Please Enter Your Credential",
                    Snackbar.LENGTH_LONG)

                    .show()
            }
        }
        LoginBinding.progressBar3.setVisibility(View.GONE)
        return LoginBinding.root
    }


}
