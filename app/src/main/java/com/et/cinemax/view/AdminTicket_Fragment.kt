package com.et.cinemax.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.et.cinemax.R
import com.et.cinemax.data.CineDatabase
import com.et.cinemax.Repo.TicketRepo
import com.et.cinemax.Repo.UserRepo
import com.et.cinemax.TicketAdapter
import com.et.cinemax.viewModel.TicketViewModel
import kotlinx.android.synthetic.main.fragment_adminticket.view.*
import kotlinx.android.synthetic.main.fragment_my_tickets.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

lateinit var recyclervTA: RecyclerView
lateinit var ticketViewModelA: TicketViewModel
class AdminTicket_Fragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.fragment_adminticket, container, false)
        val ticketDao = CineDatabase.getDatabase(this.context!!).TicketDao()
        val ticketRepo = TicketRepo(ticketDao)
        view.progressBarAdminTick.setVisibility(View.VISIBLE)
        GlobalScope.launch {
            try {
                val AdminTicketsFromNet=ticketRepo.FetchAllTicketsFromNet()
                val RespMyTicket=AdminTicketsFromNet.execute().body()
                ticketRepo.DeleteAll()
                if(RespMyTicket!=null)
                    for (ticket in RespMyTicket){
                        ticketDao.save(ticket)
                    }
            }
            catch (ex: Exception){
                ex.printStackTrace()
                activity!!.runOnUiThread {
                    Toast.makeText(activity,"Unable to fetch your Tickets...",Toast.LENGTH_LONG).show()
                }
            }
        }
        return view
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        GlobalScope.launch {
            Thread.sleep(5000)
            activity!!.runOnUiThread {
                ticketViewModelA = ViewModelProviders.of(this@AdminTicket_Fragment).get(TicketViewModel::class.java)
                val ticketsadapter = TicketAdapter(this@AdminTicket_Fragment.context!!)
                ticketViewModelA.mytickets.observe(this@AdminTicket_Fragment, Observer {
                    ticketsadapter.setTicketList(it)
                })
                recyclervTA=view.admin_Ticket_recycler
                recyclervTA.layoutManager= LinearLayoutManager(this@AdminTicket_Fragment.context)
                val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
                recyclervTA.setLayoutAnimation(controller)
                recyclervTA.adapter=ticketsadapter
                view.progressBarAdminTick.setVisibility(View.GONE)
            }
        }
    }




}
