package com.et.cinemax.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.et.cinemax.viewModel.MovieViewModel
import com.et.cinemax.viewModel.UserViewModel
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.et.cinemax.MovieAdapter
import com.et.cinemax.R
import com.et.cinemax.data.User
import com.et.cinemax.databinding.FragmentMovieListBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */

class MovieList : Fragment() {
    lateinit var recyclerv: RecyclerView
    lateinit var movieViewModel: MovieViewModel
    lateinit var userViewModel1: UserViewModel
    lateinit var userForView: User
    lateinit var movieAdapter:MovieAdapter
    lateinit var MovieListBinding:FragmentMovieListBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanqceState: Bundle?
    ): View? {
        MovieListBinding = DataBindingUtil.inflate<FragmentMovieListBinding>(inflater,R.layout.fragment_movie_list,container, false)
        userViewModel1 = ViewModelProviders.of(this).get(UserViewModel::class.java)
        MovieListBinding.setLifecycleOwner(this)
        recyclerv=MovieListBinding.recyclerMovieCards
        recyclerv.layoutManager=GridLayoutManager(activity!!,2)
        movieAdapter= MovieAdapter(activity!!)
        movieViewModel = ViewModelProviders.of(this).get(MovieViewModel::class.java)

        val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
        recyclerv.setLayoutAnimation(controller)
        recyclerv.adapter=movieAdapter
        return MovieListBinding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userViewModel1.getUserInfo().observe(this, Observer {
            if (it!=null){
                MovieListBinding.usermovielistBinding=it!!
            }
        })
        movieViewModel.allMovies.observe(this, Observer { movies->
            movieAdapter.setMovieList(movies)
        })
    }
}



