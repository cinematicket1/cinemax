package com.et.cinemax.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.et.cinemax.*
import kotlinx.android.synthetic.main.fragment_admin_logged_in.view.*




// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class AdminLoggedIn : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view=inflater.inflate(R.layout.fragment_admin_logged_in, container, false)

        val adapter = MyViewPagerAdapter(activity!!.supportFragmentManager)
        adapter.addFragment(AdminMovie_Fragment(), " Movie")
        adapter.addFragment(AdminTicket_Fragment(), " Ticket")
        adapter.addFragment(Adminuser_Fragment(), " Payment")
        adapter.addFragment(AdminPayment_Fragment(), " User ")
        view.viewPager.adapter = adapter

        val tabLayout = view.tabberScroll
        tabLayout.setupWithViewPager(view.viewPager)
        //view.tabs.setupWithViewPager(view.viewPager)
        return view
    }


}
