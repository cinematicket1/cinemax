package com.et.cinemax.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.et.cinemax.R
import com.et.cinemax.data.CineDatabase
import com.et.cinemax.Repo.TicketRepo
import com.et.cinemax.Repo.UserRepo
import com.et.cinemax.TicketAdapter
import com.et.cinemax.databinding.FragmentMyTicketsBinding
import com.et.cinemax.viewModel.TicketViewModel
import com.et.cinemax.viewModel.UserViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"



class MyTickets : Fragment() {
    lateinit var recyclervT: RecyclerView
    lateinit var ticketViewModel: TicketViewModel
    lateinit var userViewModelTick: UserViewModel
    lateinit var MyTicketsBinding:FragmentMyTicketsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        MyTicketsBinding=DataBindingUtil.inflate<FragmentMyTicketsBinding>(inflater,
            R.layout.fragment_my_tickets,container, false)
        val ticketDao = CineDatabase.getDatabase(this.context!!).TicketDao()
        val userDao = CineDatabase.getDatabase(this.context!!).UserDao()
        val ticketRepo = TicketRepo(ticketDao)
        val userRepo=UserRepo(userDao)
        MyTicketsBinding.noticketsText.setVisibility(View.GONE)
        MyTicketsBinding.progressBarLoadTick.setVisibility(View.VISIBLE)
        GlobalScope.launch {
            try {
                val callUsID=userRepo.DirectRepoGetUserFromDatabase().phonenumber
                val myTicketsFromNet=ticketRepo.getMyTickets(Integer.parseInt(callUsID))
                val RespMyTicket=myTicketsFromNet.execute().body()
                if(RespMyTicket!=null)
                    ticketRepo.DeleteAll()
                    for (ticket in RespMyTicket!!){
                        ticketDao.save(ticket)
                    }
            }
            catch (ex: Exception){
                ex.printStackTrace()
                activity!!.runOnUiThread {
                    Toast.makeText(activity,"Unable to fetch your Tickets...",Toast.LENGTH_LONG).show()
                }
            }
        }
        return MyTicketsBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        GlobalScope.launch {
            Thread.sleep(5000)
            activity!!.runOnUiThread {
            ticketViewModel = ViewModelProviders.of(this@MyTickets).get(TicketViewModel::class.java)
            val ticketsadapter = TicketAdapter(this@MyTickets.context!!)
            ticketViewModel.mytickets.observe(this@MyTickets, Observer {
                ticketsadapter.setTicketList(it)
            }
            )
            recyclervT=MyTicketsBinding.ticketRecycler
            recyclervT.layoutManager= LinearLayoutManager(this@MyTickets.context)
            val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
            recyclervT.setLayoutAnimation(controller)
            recyclervT.adapter=ticketsadapter
                if(ticketsadapter.getItemCount()<1){
                    MyTicketsBinding.noticketsText.setVisibility(View.VISIBLE)
                }
                else{
                    MyTicketsBinding.noticketsText.setVisibility(View.GONE)
                }
            MyTicketsBinding.progressBarLoadTick.setVisibility(View.GONE)
            }
        }
    }

    override fun onDetach() {
        super.onDetach()
        val ticketDao = CineDatabase.getDatabase(this.context!!).TicketDao()
        val ticketRepo = TicketRepo(ticketDao)
        GlobalScope.launch {
            ticketRepo.DeleteAll()
        }
    }
}
