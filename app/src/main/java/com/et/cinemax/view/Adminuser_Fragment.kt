package com.et.cinemax.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.et.cinemax.R
import com.et.cinemax.data.CineDatabase
import com.et.cinemax.data.User
import com.et.cinemax.Repo.UserRepo
import kotlinx.android.synthetic.main.fragment_adminuser.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception

class Adminuser_Fragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.fragment_adminuser, container, false)
        view.Add_balance_button.setOnClickListener {
            val amount=view.Add_balance_amount.text.toString().toInt()
            val userPhone=view.Add_Balance_phone_number.text.toString()
            val AddBalTransactionUser= User(0,0,"","",userPhone,"",amount)
            val userDao= CineDatabase.getDatabase(activity!!).UserDao()
            val userRepo= UserRepo(userDao)
            GlobalScope.launch {
                try {
            val addBalanceResp=userRepo.AddBalance(AddBalTransactionUser).execute().body()
            if (addBalanceResp.equals("1")){
                activity!!.runOnUiThread{
                    Toast.makeText(activity,"SuccessFully Added Balance",
                        Toast.LENGTH_LONG).show()
                }
            }
            else{
                activity!!.runOnUiThread{
                Toast.makeText(activity,"Cannot Add",
                    Toast.LENGTH_LONG).show()
                }
            }
            }
            catch(exx: Exception){
                activity!!.runOnUiThread{
                Toast.makeText(activity,"Connection Timeout",
                    Toast.LENGTH_LONG).show()
                }
            }
        }
        }
        return view
    }



}
