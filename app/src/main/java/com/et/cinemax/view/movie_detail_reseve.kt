package com.et.cinemax.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.et.cinemax.R
import com.et.cinemax.Repo.SeatRepo
import com.et.cinemax.Repo.TicketRepo
import com.et.cinemax.Repo.UserRepo
import com.et.cinemax.data.*
import com.et.cinemax.databinding.FragmentMovieDetailReseveBinding
import com.et.cinemax.viewModel.MovieViewModel
import com.et.cinemax.viewModel.SeatsViewModel
import com.et.cinemax.viewModel.UserViewModel
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_movie_detail_reseve.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception




class movie_detail_reseve : Fragment() {

    lateinit var MovieID:String
    lateinit var movieViewModel: MovieViewModel
    lateinit var seatViewModel: SeatsViewModel
    lateinit var userViewModel: UserViewModel
    lateinit var MovieDetailBinding:FragmentMovieDetailReseveBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        MovieDetailBinding=DataBindingUtil.inflate<FragmentMovieDetailReseveBinding>(inflater,
            R.layout.fragment_movie_detail_reseve,container, false)
        MovieDetailBinding.setLifecycleOwner(this.activity)
        val idS=arguments?.getString("MovieID")
        val intID=if (idS!=null) idS.toInt() else 0
        val seatDao=CineDatabase.getDatabase(activity!!).SeatsDao()
        val seatRepo= SeatRepo(seatDao)
        GlobalScope.launch {
            try{
            val seat=seatRepo.getSeatFromNet(intID).execute().body()
                if (seat!=null){
                    seatRepo.save(seat)
                }}
            catch(ex:Exception){
                activity!!.runOnUiThread {
                    Toast.makeText(activity,"Error in Getting Info",Toast.LENGTH_LONG).show()
                }
            }
        }
        MovieID=intID.toString()
        seatViewModel=ViewModelProviders.of(this).get(SeatsViewModel::class.java)
        seatViewModel.getMovieInfoById(intID).observe(this, Observer {
            it?.let {
                MovieDetailBinding.seatsBinding=it!!
            }
        })
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        MovieDetailBinding.lifecycleOwner=viewLifecycleOwner
        movieViewModel = ViewModelProviders.of(activity!!).get(MovieViewModel::class.java)
        movieViewModel.getMovieInfoById(intID).observe(this, Observer {movieDet->
            movieDet?.let {
                MovieDetailBinding.movieDetailBinding=movieDet
                Picasso.get()
                    .load(movieDet.imglink+".jpg")
                    .fit()
                    .into(MovieDetailBinding.movieDetailImageReserve)
            }

        })

        MovieDetailBinding.progressBarReserveLoad.setVisibility(View.INVISIBLE)
        MovieDetailBinding.toggleButton3.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("1")
        }
        MovieDetailBinding.toggleButton7.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("2")
        }
        MovieDetailBinding.toggleButton8.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("3")
        }
        MovieDetailBinding.toggleButton4.setOnClickListener() {
            MovieDetailBinding.movieDetailSeatInput.setText("4")
        }
        MovieDetailBinding.toggleButton5.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("5")
        }
        MovieDetailBinding.toggleButton10.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("6")
        }
        MovieDetailBinding.toggleButton7.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("7")
        }
        MovieDetailBinding.toggleButton8.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("8")
        }
        MovieDetailBinding.toggleButton9.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("9")
        }
        MovieDetailBinding.toggleButton10.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("10")
        }
        MovieDetailBinding.toggleButton11.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("11")
        }
        MovieDetailBinding.toggleButton12.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("12")
        }
        MovieDetailBinding.toggleButton13.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("13")
        }
        MovieDetailBinding.toggleButton14.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("14")
        }
        MovieDetailBinding.toggleButton15.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("15")
        }
        MovieDetailBinding.toggleButton16.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("16")
        }
        MovieDetailBinding.toggleButton17.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("17")
        }
        MovieDetailBinding.toggleButton18.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("18")
        }
        MovieDetailBinding.toggleButton19.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("19")
        }
        MovieDetailBinding.toggleButton20.setOnClickListener(){
            MovieDetailBinding.movieDetailSeatInput.setText("20")
        }
        MovieDetailBinding.ReserveButton.setOnClickListener {
            MovieDetailBinding.progressBarReserveLoad.setVisibility(View.VISIBLE)
            var reqUser: User?=null
            userViewModel.user.observe(this, Observer { user ->
                reqUser=user
            })
            GlobalScope.launch {
                try {
                    val seatChoice = MovieDetailBinding.movieDetailSeatInput.text.toString().toInt()
                    val movieChoice = MovieID.toInt()
                    val RequestedTicket: Ticket = Ticket(
                        tid = 0,
                        uid = Integer.parseInt(reqUser!!.phonenumber),
                        mid = movieChoice,
                        seatno = seatChoice,
                        pid = 0,
                        date_ordered = null,
                        used = 0
                    )

                    val ticketDao = CineDatabase.getDatabase(activity!!).TicketDao()
                    val ticketRepo = TicketRepo(ticketDao)
                    val UTBind = TicketUserBondModel(reqUser!!, RequestedTicket)
                    val orderResponse = ticketRepo.makeOrderRequest(UTBind).execute().body()
                    var orderResponseToMessage: String? = null
                    if (orderResponse != null) {
                        if (orderResponse.equals("1")) {
                            orderResponseToMessage = "Succesfull"
                        }
                        if (orderResponse.equals("2")) {
                            orderResponseToMessage = "Too Late Seat is taken Just Now"
                        }
                        if (orderResponse.equals("3")) {
                            orderResponseToMessage = "Your Balance is Insufficent"
                        }
                        if (orderResponse.equals("4")) {
                            orderResponseToMessage = "Incorrect Data Given"
                        }
                        if (orderResponse.equals("5")) {
                            orderResponseToMessage = "Incorrect Data Given"
                        }
                        activity!!.runOnUiThread {
                            MovieDetailBinding.progressBarReserveLoad.setVisibility(View.INVISIBLE)
                        }
                        Snackbar.make(
                            snack_movie_detail_reserve,
                            "Message - " + orderResponseToMessage.toString(),
                            Snackbar.LENGTH_LONG
                        )
                            .setBackgroundTint(resources.getColor(R.color.colorAccent))
                            .setTextColor(resources.getColor(R.color.colorPrimaryDark))
                            .show()
                        val userDao = CineDatabase.getDatabase(activity!!).UserDao()
                        val userRepo = UserRepo(userDao)
                        val PhoneInDb: String = userRepo.DirectRepoGetUserFromDatabase().phonenumber
                        val PassInDb: String = userRepo.DirectRepoGetUserFromDatabase().password
                        val InfoReq = userRepo.getUserInfoFromNet(PhoneInDb)
                        val userInfoFinal = InfoReq.execute().body()
                        userRepo.DeleteAll()
                        userInfoFinal!!.password = PassInDb
                        userRepo.SaveUserInfo(userInfoFinal)
                        val TestPhoneInDb: String = userRepo.DirectRepoGetUserFromDatabase().phonenumber
                        val TestBalInDb: String = userRepo.DirectRepoGetUserFromDatabase().cbalance.toString()
                        activity!!.runOnUiThread {
                            Toast.makeText(activity, "Deducted and Updated", Toast.LENGTH_LONG).show()
                            Toast.makeText(
                                activity,
                                "Updated info = ${TestPhoneInDb} +  Bal= ${TestBalInDb}",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                } catch (ex: Exception) {
                    activity!!.runOnUiThread {
                        MovieDetailBinding.progressBarReserveLoad.setVisibility(View.INVISIBLE)
                    }
                    ex.printStackTrace()
                    Snackbar.make(
                        snack_movie_detail_reserve,
                        "An Error Occured While Ordering Your Ticket",
                        Snackbar.LENGTH_LONG
                    )
                        .setBackgroundTint(resources.getColor(R.color.colorAccent))
                        .setTextColor(resources.getColor(R.color.colorPrimaryDark))
                        .show()
                }
            }
        }
        return MovieDetailBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userViewModel.user.observe(this, Observer {
            if (it!=null){
            MovieDetailBinding.userBinding=it!!
            }
        })
    }


}
