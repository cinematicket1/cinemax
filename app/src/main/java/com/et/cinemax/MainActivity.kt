package com.et.cinemax

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Layout
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.navigation.NavOptions
import androidx.navigation.Navigation.findNavController
import com.et.cinemax.data.Movie
import com.et.cinemax.data.User
import com.et.cinemax.Repo.MovieRepo
import com.et.cinemax.Repo.TicketRepo
import com.et.cinemax.Repo.UserRepo
import com.et.cinemax.data.CineDatabase
import com.et.cinemax.tasks.backgroundRefresh
import com.et.cinemax.viewModel.UserViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_loading.*
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_movie_list.*
import kotlinx.android.synthetic.main.fragment_sign_up.*
import kotlinx.android.synthetic.main.one_movie_view.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {
    val act:Activity=this
    val Loggedin:Int=0
    lateinit var testBtn:Button
    val INTERNET_REQUEST=101
    val backgroundRefresher=backgroundRefresh()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val permission = ContextCompat.checkSelfPermission(this,
            Manifest.permission.INTERNET)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.INTERNET),
                INTERNET_REQUEST)
        }
        val userDao= CineDatabase.getDatabase(this).UserDao()
        val userRepo=UserRepo(userDao)
        GlobalScope.launch{
            var user:User?=null
            //userRepo.SaveUserInfo(User(3,0,"TEST","T","0922468142","a",0))
            user=userRepo.DirectRepoGetUserFromDatabase()
            runOnUiThread{
                if(connected()){
                    if(user==null){
                        val navController1 = findNavController(act,R.id.main_frame)
                        navController1.navigate(R.id.action_loadingFragment_to_loginFragment, null,
                            NavOptions.Builder()
                                .setPopUpTo(R.id.loadingFragment,true).build())
                        //Toast.makeText(act,"User Is Null",Toast.LENGTH_LONG).show()
                    }
                    else{
                        //Toast.makeText(this,"From DB= "+user.phonenumber+"Pass "+user.password,Toast.LENGTH_LONG).show()
                        FinalLogin(user!!.phonenumber,user!!.password)
                    }
                }
                else{
                    val navControllerError = findNavController(act,R.id.main_frame)
                    navControllerError.navigate(R.id. action_loadingFragment_to_connectionError, null,
                        NavOptions.Builder()
                            .setPopUpTo(R.id.loadingFragment,true).build())
                }
            }
        }
        GlobalScope.launch {
            backgroundRefresher.StartBackgroundTask(application,act)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        backgroundRefresher.StopBackgroundTask()
    }


    override fun onSupportNavigateUp() = findNavController(this, R.id.main_frame).navigateUp()

    fun toSignUp(view: View){
        val navController1 = findNavController(this,R.id.main_frame)
        navController1.navigate(R.id.action_loginFragment_to_signUpFragment)
    }
    fun ToMovieDetail(view: View){
        val SelectedID=view.text_view_id_hide.text
        Toast.makeText(this,SelectedID,Toast.LENGTH_LONG).show()
        var bundle= bundleOf("MovieID" to SelectedID)
        val navController1 = findNavController(this,R.id.main_frame)
        navController1.navigate(R.id.action_movieList_to_movie_detail_reseve,bundle)
    }
    fun toSettings(view: View){
        val navController1 = findNavController(this,R.id.main_frame)
        try {
            navController1.navigate(R.id.action_movieList_to_settingsFragment)
        }
        catch (ex : Exception){
            navController1.navigate(R.id.action_movie_detail_reseve_to_settingsFragment)
        }
    }
    fun toMyTickets(view: View){
        val navController1 = findNavController(this,R.id.main_frame)
        try {
            navController1.navigate(R.id.action_movieList_to_myTickets)
        }
        catch (ex : Exception){
            navController1.navigate(R.id.action_movie_detail_reseve_to_myTickets)
        }
    }
    fun TestButton(view: View){
        Toast.makeText(this,"Test",Toast.LENGTH_LONG).show()
    }
    fun toLogin(view: View){
        val navController1 = findNavController(this,R.id.main_frame)
        navController1.navigate(R.id.action_signUpFragment_to_loginFragment)
    }
    fun Logout(view: View){
        val userDao=CineDatabase.getDatabase(this).UserDao()
        val userRepo=UserRepo(userDao)
        AsyncTask.execute{
            userRepo.DeleteAll()
        }
        val loggedOutAct :Intent=Intent(this, MainActivity::class.java)
        this.finish()
        startActivity(loggedOutAct)
    }
    private fun connected():Boolean {

        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected

    }
    fun DeleteAllTest(view: View){
        val movieDao= CineDatabase.getDatabase(applicationContext).movieDao()
        val movieRepo= MovieRepo(movieDao)
        AsyncTask.execute{
            movieRepo.clearAll()
        }
        Toast.makeText(this,"Deleted All",Toast.LENGTH_LONG).show()
    }
    fun FinalLogin(username:String,password:String){
        if (connected()){
            try {
                progressBar3.setVisibility(View.VISIBLE)
            }
            catch(ex: Exception){

            }
            val finalUser=User(0,5,"","",username,password,0)
            var checker:Int?
            checker=0
            GlobalScope.launch{
                val userDao=CineDatabase.getDatabase(applicationContext).UserDao()
                val userRepo=UserRepo(userDao)
                val userLog=userRepo.RequestAuth(finalUser)
                try{
                    val IntMessage=userLog.execute().body()
                    checker=IntMessage
                    if(checker!=0 && checker!=null){
                        val InfoReq=userRepo.getUserInfoFromNet(checker!!.toInt())
                        val userInfoFinal=InfoReq.execute().body()
                        if(userInfoFinal!!.role==0){
                            val movieDao= CineDatabase.getDatabase(applicationContext).movieDao()
                            val movieRepo= MovieRepo(movieDao)
                            movieRepo.clearAll()
                            val FetchedMovies:List<Movie>? = movieRepo.getAllMoviesFromNet().execute().body()
                            FetchedMovies?.let {
                                for(Mov in FetchedMovies){
                                    movieRepo.Save(Mov)
                                }
                            }
                            runOnUiThread {
                                val navController = findNavController(act, R.id.main_frame)
                                try{
                                    navController.navigate(
                                        R.id.action_loginFragment_to_movieList2, null,
                                        NavOptions.Builder()
                                            .setPopUpTo(R.id.loginFragment, true).build()
                                    )}
                                catch (ex:Exception){
                                    navController.navigate(
                                        R.id.action_loadingFragment_to_movieList, null,
                                        NavOptions.Builder()
                                            .setPopUpTo(R.id.loadingFragment, true).build())
                                }
                                try {
                                    progressBar3.setVisibility(View.GONE)
                                }
                                catch (ex:Exception){
                                }
                            }
                            userRepo.DeleteAll()
                            userInfoFinal.password=finalUser.password
                            val UserTemp=userRepo.SaveUserInfo(userInfoFinal)
                        }
                        else{
                            runOnUiThread {
                                val navController = findNavController(act, R.id.main_frame)
                                navController.navigate(
                                    R.id.action_loginFragment_to_adminLoggedIn2, null,
                                    NavOptions.Builder()
                                        .setPopUpTo(R.id.loginFragment, true).build()
                                )
                                progressBar3.setVisibility(View.GONE)
                            }
                        }
                    }
                    else{
                        runOnUiThread {
                            try {
                                Snackbar.make(login_fragment, "Incorrect Phone or Password", Snackbar.LENGTH_LONG)
                                    .setBackgroundTint(resources.getColor(R.color.colorAccent))
                                    .setTextColor(resources.getColor(R.color.colorPrimaryDark))
                                    .show()
                            }
                            catch(exx : Exception){
                                Snackbar.make(loading_frag, "Incorrect Phone or Password", Snackbar.LENGTH_LONG)
                                    .setBackgroundTint(resources.getColor(R.color.colorAccent))
                                    .setTextColor(resources.getColor(R.color.colorPrimaryDark))
                                    .show();
                            }
                            try {
                                val navController1 = findNavController(act, R.id.main_frame)
                                navController1.navigate(
                                    R.id.action_loadingFragment_to_loginFragment, null,
                                    NavOptions.Builder()
                                        .setPopUpTo(R.id.loadingFragment, true).build()
                                )
                                try {
                                    progressBar3.setVisibility(View.INVISIBLE)
                                }
                                catch (ex:Exception){

                                }
                            }
                            catch (ex:Exception){

                            }
                        }
                    }
                    runOnUiThread{
                        //Toast.makeText(this,"RespID-"+IntMessage?.toInt().toString(),Toast.LENGTH_LONG).show()
                        try {
                            progressBar3.setVisibility(View.INVISIBLE)
                        }
                        catch (ex:Exception){

                        }
                    }
                }
                catch(ex:Exception){
                    ex.printStackTrace()
                    runOnUiThread {
                        try {
                            ex.printStackTrace()
                            Snackbar.make(login_fragment, "Error Connecting to Server...", Snackbar.LENGTH_LONG)
                                .setBackgroundTint(resources.getColor(R.color.colorAccent))
                                .setTextColor(resources.getColor(R.color.colorPrimaryDark))
                                .show()
                        }
                        catch(Ex: Exception){
                            Snackbar.make(loading_frag, "Error Connecting to Server...", Snackbar.LENGTH_LONG)
                                .setBackgroundTint(resources.getColor(R.color.colorAccent))
                                .setTextColor(resources.getColor(R.color.colorPrimaryDark))
                                .show();
                        }
                        val navController1 = findNavController(act, R.id.main_frame)
                        try {
                            navController1.navigate(
                                R.id.action_loadingFragment_to_connectionError, null,
                                NavOptions.Builder()
                                    .setPopUpTo(R.id.loadingFragment, true).build()
                            )
                        }
                        catch(Ex : Exception){
                        }
                        try {
                            progressBar3.setVisibility(View.INVISIBLE )
                        }
                        catch (ex:Exception){
                        }

                    }
                }
            }
        }
        else{
            try {
                Snackbar.make(login_fragment, "No Connection...", Snackbar.LENGTH_LONG)
                    .setBackgroundTint(resources.getColor(R.color.colorAccent))
                    .setTextColor(resources.getColor(R.color.colorPrimaryDark))
                    .show()
            }
            catch(Ex: Exception){
                Snackbar.make(loading_frag, "No Connection...", Snackbar.LENGTH_LONG)
                    .setBackgroundTint(resources.getColor(R.color.colorAccent))
                    .setTextColor(resources.getColor(R.color.colorPrimaryDark))
                    .show()
            }
        }
    }
    fun SignUp(view:View){
        progressBar4.setVisibility(View.VISIBLE)
        if (connected()){
            val UserToSignUp:User=User(
                uid = null,
                role=0,
                fullname = sign_up_name_input.text.toString(),
                email = sign_up_email.text.toString(),
                password = sign_up_password.text.toString(),
                cbalance = 0,
                phonenumber =sign_up_phone.text.toString())
            val userDao=CineDatabase.getDatabase(this).UserDao()
            val userRepo=UserRepo(userDao)
            GlobalScope.launch{
                val userSignUp=userRepo.SignUp(UserToSignUp)
                try{
                    val SignUpResposnse=userSignUp.execute().body()
                    runOnUiThread {
                        if(SignUpResposnse.toString().equals("1")){
                            Snackbar.make(sign_upfrag, "Succesfully Signed up", Snackbar.LENGTH_LONG)
                                .setBackgroundTint(resources.getColor(R.color.colorAccent))
                                .setTextColor(resources.getColor(R.color.colorPrimaryDark))
                                .show()
                        }
                        else{
                            Snackbar.make(sign_upfrag, "Phone number is already in use", Snackbar.LENGTH_LONG)
                                .setBackgroundTint(resources.getColor(R.color.colorAccent))
                                .setTextColor(resources.getColor(R.color.colorPrimaryDark))
                                .show()
                        }
                        progressBar4.setVisibility(View.INVISIBLE)
                    }
                }
                catch (timeOut: Exception){
                    runOnUiThread {
                        Snackbar.make(sign_upfrag, "Error Connecting to Server...", Snackbar.LENGTH_LONG)
                            .setBackgroundTint(resources.getColor(R.color.colorAccent))
                            .setTextColor(resources.getColor(R.color.colorPrimaryDark))
                            .show()
                        progressBar4.setVisibility(View.INVISIBLE)
                    }
                }
            }
        }
    }
    fun reserveTicket(view: View){

    }
}