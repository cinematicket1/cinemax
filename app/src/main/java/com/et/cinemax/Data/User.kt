package com.et.cinemax.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "user")
class User (@PrimaryKey @ColumnInfo(name="uid")val uid: Int?,
            val role:Int,
            val fullname: String,
            val email:String,
            val phonenumber:String,
            var password:String,
            val cbalance:Int): Serializable{
}