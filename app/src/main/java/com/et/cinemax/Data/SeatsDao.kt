package com.et.cinemax.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface SeatsDao {
    @Insert
    fun save(seats: Seats):Long
    @Query("select * from seats where mid=:Sid")
    fun getSeatByMid(Sid:Int):LiveData<Seats>
}