package com.et.cinemax.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "ticket")
class Ticket(@PrimaryKey @ColumnInfo(name="tid")val tid: Int,
             @ColumnInfo(name="mid") val mid: Int,
             @ColumnInfo(name="uid") val uid: Int?,
             @ColumnInfo(name="seatno") val seatno: Int,
             @ColumnInfo(name="pid") val pid: Int,
             @ColumnInfo(name="date_ordered") val date_ordered: String?,
             @ColumnInfo(name="used") val used: Int) {}

