package com.et.cinemax.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "seats")
class Seats(@PrimaryKey @ColumnInfo(name="sid")val sid: Int,
            val mid:Int,
            val s1:Int,
            val s2:Int,
            val s3:Int,
            val s4:Int,
            val s6:Int,
            val s7:Int,
            val s8:Int,
            val s9:Int,
            val s10:Int,
            val s11:Int,
            val s12:Int,
            val s13:Int,
            val s14:Int,
            val s15:Int,
            val s16:Int,
            val s17:Int,
            val s18:Int,
            val s19:Int,
            val s20:Int){

}