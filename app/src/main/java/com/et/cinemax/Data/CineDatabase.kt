package com.et.cinemax.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Movie::class,Payment::class,Seats::class,Ticket::class,User::class),version=5)
abstract class CineDatabase: RoomDatabase() {
    abstract fun movieDao():MovieDao
    abstract fun paymentDao():PaymentDao
    abstract fun SeatsDao():SeatsDao
    abstract fun TicketDao():TicketDao
    abstract fun UserDao():UserDao

    companion object {
        @Volatile
        private var INSTANCE :CineDatabase?=null

        fun getDatabase(context: Context):CineDatabase{
            val tempInstance= INSTANCE
            if(tempInstance!=null){
                return tempInstance
            }
            synchronized(this){
                val instance= Room.databaseBuilder(
                    context.applicationContext,
                    CineDatabase::class.java,"cine_database")
                    .fallbackToDestructiveMigration()
                    .build()
                return instance
            }
        }
    }
}