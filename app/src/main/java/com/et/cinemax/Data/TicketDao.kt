package com.et.cinemax.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface TicketDao {
    @Query("SELECT * FROM TICKET")
    fun getTickets():LiveData<List<Ticket>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(ticket: Ticket): Long

    @Query("Delete from ticket")
    fun deleteAll()
}