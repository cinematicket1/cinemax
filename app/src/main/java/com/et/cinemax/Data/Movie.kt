package com.et.cinemax.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "Movies")
class Movie(
    @PrimaryKey @ColumnInfo(name="mid")val mid: Int,
    @ColumnInfo(name="name") val name: String,
    @ColumnInfo(name="genre") val genre: String,
    @ColumnInfo(name="language") val language: String,
    @ColumnInfo(name="amount") val amount: String,
    @ColumnInfo(name="show_time") val showtime: String,
    @ColumnInfo(name="show_date") val showdate: String,
    @ColumnInfo(name="seatId") val seatid: String,
    @ColumnInfo(name="duration") val duration: String,
    @ColumnInfo(name="imglink") val imglink:String,
    @ColumnInfo(name="description") val description:String):Serializable {

}