package com.et.cinemax.tasks

import android.app.Activity
import android.app.Application
import android.widget.Toast
import androidx.annotation.WorkerThread
import com.et.cinemax.Repo.MovieRepo
import com.et.cinemax.Repo.UserRepo
import com.et.cinemax.data.CineDatabase
import com.et.cinemax.data.Movie
import com.et.cinemax.data.User
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception


class backgroundRefresh() {
    private var Refresher:Boolean=true

    @WorkerThread
    fun StartBackgroundTask(app:Application,activity: Activity){
        val db= CineDatabase.getDatabase(app)
        val userRepo= UserRepo(db.UserDao())
        val movieRepo= MovieRepo(db.movieDao())
        while(Refresher) {
            try{
            Thread.sleep(30000)
            val user: User =userRepo.DirectRepoGetUserFromDatabase()
            if (user!=null){
                val userPass=user.password
                val updatedUser=userRepo.getUserInfoFromNet(user.phonenumber).execute().body()
                if (updatedUser!=null){
                    userRepo.DeleteAll()
                    updatedUser.password=userPass
                    userRepo.SaveUserInfo(updatedUser)
                }
            }
            val FetchedMovies:List<Movie>? = movieRepo.getAllMoviesFromNet().execute().body()
            FetchedMovies?.let {
                movieRepo.clearAll()
                for(Mov in FetchedMovies){
                    movieRepo.Save(Mov)
                }
            }
            activity.runOnUiThread{
                Toast.makeText(app, "Refreshing...", Toast.LENGTH_SHORT).show()
            }
        }
            catch (ex:Exception){
                activity.runOnUiThread{
                    Toast.makeText(app, "Could not refresh.....", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    fun StopBackgroundTask(){
        this.Refresher=false
    }
}