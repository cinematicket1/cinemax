package com.et.cinemax

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.Toast
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.one_movie_view.view.*
import androidx.recyclerview.widget.RecyclerView
import com.et.cinemax.data.Movie
import com.squareup.picasso.Picasso


class MovieAdapter(val context:Context): RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    private var moviesL:List<Movie> = emptyList()

    internal fun setMovieList(movies:List<Movie>){
        this.moviesL=movies
        notifyDataSetChanged()
        Toast.makeText(context,"DataSet",Toast.LENGTH_LONG).show()
    }
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MovieViewHolder {
        val inflater=LayoutInflater.from(p0.context)
        val recyclerViewItem=inflater.inflate(R.layout.one_movie_view,p0,false)
        return MovieViewHolder(recyclerViewItem)
    }

    override fun getItemCount(): Int {
        return moviesL.size
    }

    override fun onBindViewHolder(p0: MovieViewHolder, p1: Int) {
        val mov=moviesL[p1]
        Picasso.get()
            .load(mov.imglink+".jpg")
            .fit()
            .into(p0.itemView.movie_detail_image);
        //Picasso.get().load("https://cdn1.imggmi.com/uploads/2019/6/19/e0c3b66dafa7db31cfa6efb8a53c116a-full.jpg").into(p0.itemView.movie_detail_image)
        p0.itemView.movie_detail_title.text=mov.name
        //p0.itemView.movie_detail_language.text="Lang : "+mov.language
        p0.itemView.movie_detail_amount.text=mov.amount+"Birr"
        p0.itemView.movie_detail_time.text=mov.showtime
        //p0.itemView.movie_detail_genre.text="Genre : "+mov.genre
        p0.itemView.text_view_id_hide.text=mov.mid.toString()
        p0.itemView.text_view_id_hide.setVisibility(View.GONE)
        //p0.itemView.movie_detail_image.setImageResource(R.drawable.avengersend)
        setAnimation(p0.itemView, p1);

    }
    private fun setAnimation(viewToAnimate: View, position: Int) {
        val anim =
            ScaleAnimation(0.5f, 1.0f, 0.5f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        anim.duration =  200
        viewToAnimate.startAnimation(anim)
    }

    class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}