package com.et.cinemax

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.et.cinemax.data.Ticket
import kotlinx.android.synthetic.main.one_ticket_view.view.*

class TicketAdapter(val context: Context): RecyclerView.Adapter<TicketAdapter.TicketViewHolder>() {

    private var tickets= emptyList<Ticket>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TicketAdapter.TicketViewHolder {
        val inflater= LayoutInflater.from(parent.context)
        val recyclerViewItem=inflater.inflate(R.layout.one_ticket_view,parent,false)
        return TicketAdapter.TicketViewHolder(recyclerViewItem)
    }
    fun setTicketList(tick : List<Ticket>){
        this.tickets=tick
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
            return tickets.size
        }

    override fun onBindViewHolder(holder: TicketAdapter.TicketViewHolder, position: Int) {
        val tick=tickets[position]
        holder.itemView.ticket_id.text="C" + tick.tid.toString()+"i"+tick.mid+"X"+tick.seatno

    }
    class TicketViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}