package com.et.cinemax.Repo

import androidx.lifecycle.LiveData
import com.et.cinemax.data.Movie
import com.et.cinemax.data.MovieDao
import com.et.cinemax.network.ApiServices
import retrofit2.Call

class MovieRepo(private val movieDao: MovieDao) {
    val apiServ:ApiServices= ApiServices.getInstance()
    fun getMovies():LiveData<List<Movie>>{
       return movieDao.getMovies()
    }

    fun getById(id: Int): LiveData<Movie> {
        return movieDao.getById(id)
    }

    fun getAllMoviesFromNet(): Call<List<Movie>> {
        return apiServ.getAllMoviesFromNet()
    }
    fun Save(mov: Movie): Long {
        return movieDao.insertMovie(mov)
    }
    fun SaveToNetwork(mov: Movie): Call<String> {
        return apiServ.AddMovie(mov)
    }
    fun clearAll(){
        movieDao.ClearAllMovies()
    }

}