package com.et.cinemax.Repo

import androidx.lifecycle.LiveData
import com.et.cinemax.data.Seats
import com.et.cinemax.data.SeatsDao
import com.et.cinemax.network.ApiServices
import retrofit2.Call

class SeatRepo (private val seatDao: SeatsDao){

    val apiServ: ApiServices = ApiServices.getInstance()
    fun save(seat:Seats):Long{
       return seatDao.save(seat)
    }
    fun getSeatByMid(mid:Int):LiveData<Seats>{
        return seatDao.getSeatByMid(mid)
    }
    fun getSeatFromNet(mid:Int): Call<Seats> {
        return apiServ.getSeatData(mid)
    }
}