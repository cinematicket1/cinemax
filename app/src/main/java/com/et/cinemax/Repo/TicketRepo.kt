package com.et.cinemax.Repo

import androidx.lifecycle.LiveData
import com.et.cinemax.data.Ticket
import com.et.cinemax.data.TicketDao
import com.et.cinemax.network.ApiServices
import com.et.cinemax.data.TicketUserBondModel
import retrofit2.Call

class TicketRepo(private val tickDao: TicketDao) {
    val apiServ: ApiServices = ApiServices.getInstance()
    fun getAllTickets():LiveData<List<Ticket>>{
        return tickDao.getTickets()
    }
    fun getMyTickets(uid: Int):Call<List<Ticket>>{
        return apiServ.getMyTickets(uid)
    }

    fun Save(ticket: Ticket): Long {
        return tickDao.save(ticket)
    }
    fun DeleteAll() {
        return tickDao.deleteAll()
    }
    fun FetchAllTicketsFromNet(): Call<List<Ticket>> {
        return apiServ.adminGetAllTickets()
    }
    fun makeOrderRequest(UTBind: TicketUserBondModel):Call<String>{
        return apiServ.Order(UTBind.UserBondModel,UTBind.TicketBondModel)
    }
}