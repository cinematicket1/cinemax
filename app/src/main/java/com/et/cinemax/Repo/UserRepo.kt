package com.et.cinemax.Repo

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.et.cinemax.data.User
import com.et.cinemax.data.UserDao
import com.et.cinemax.network.ApiServices
import retrofit2.Call

class UserRepo(private val userDao: UserDao) {
    val apiServ:ApiServices= ApiServices.getInstance()

    fun getUserFromDatabase(): LiveData<User> {
        return userDao.getUserFromDb()
    }
    fun DirectRepoGetUserFromDatabase():User {
        return userDao.DirectgetUserFromDb()
    }
    fun getUserInfoFromNet(phone:String):Call<User>{
        return apiServ.GetInfoByPhone(phone)
    }
    fun getUserInfoFromNet(id:Int):Call<User>{
        return apiServ.GetInfoById(id)
    }
    fun DeleteAll(){
        userDao.ClearAllUser()
    }
    fun SaveUserInfo(user: User):Long
    {
        return userDao.save(user)
    }
    suspend fun RequestAuth(user:User): Call<Int> {
        return apiServ.RequestAuth(user)
    }
    fun SignUp(user: User):Call<Int>{
        return apiServ.SignUp(user)
    }
    fun AddBalance(user: User):Call<String>{
        return apiServ.AddBalance(user)
    }
}