package com.et.cinemax.network

import com.et.cinemax.data.*
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface ApiServices {
    //USER Services
    @GET("user/phone/{phone}")
    fun GetInfoByPhone(@Path("phone") id:String):Call<User>
    @GET("user/{id}")
    fun GetInfoById(@Path("id") id:Int ):Call<User>
    @POST("/user/RequestAuthWith")
    fun RequestAuth(@Body user: User): Call<Int>
    @POST("/user/SignUp")
    fun SignUp(@Body user: User): Call<Int>
    /*@POST("/user/ChangePassword")
    fun ChangePassword(@Body chuser: User): Call<Int>
    */

    //MOVIE Services
    @GET("movies/0")
    fun getAllMoviesFromNet():Call<List<Movie>>
    @POST("movies/admin/addmovie")
    fun AddMovie(@Body newMov: Movie): Call<String>
    /*@POST("movies/admin/updateMovie")
    fun updateMov(@Body updMov: Movie):Call<Movie>
    */


    //SEAT Services
    @GET ("seats/getForMovie/{mid}")
    fun getSeatData(@Path("mid") mid:Int):Call<Seats>

    //TICKET Services
    @Multipart
    @POST("/ticket/Order")
    fun Order(@Part("user")reqUser:User,@Part("ticket") newTicket: Ticket):Call<String>
    @GET("/ticket/myTickets/{uid}")
    fun getMyTickets(@Path("uid") uid:Int):Call<List<Ticket>>
    @GET("/ticket/getPendingTickets")
    fun adminGetAllTickets():Call<List<Ticket>>
    @GET("/ticket/getTickForMovie/{mid}")
    fun getTickForMovie(@Path("mid") mid:Int):Call<List<Ticket>>
    /*@POST("/ticket/useTicket")
    fun useTicket()
    @POST("/ticket/cancelOrder")
    fun cancelOrder()
    */
    //PAYMENT Services
    @POST("/payments/addBalance")
    fun AddBalance(@Body addBalUser: User):Call<String>



    companion object {

        private val baseUrl = "http://192.168.43.189:10"

        fun getInstance(): ApiServices {
            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(ApiServices::class.java)
        }
}
}