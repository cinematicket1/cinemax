package com.et.cinemax.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.et.cinemax.Repo.SeatRepo
import com.et.cinemax.data.CineDatabase
import com.et.cinemax.data.Seats
import retrofit2.Call

class SeatsViewModel (application: Application) : AndroidViewModel(application){
    private val seatRepo:SeatRepo
    init {
        val seatDao= CineDatabase.getDatabase(application).SeatsDao()
        seatRepo= SeatRepo(seatDao)
    }
    fun getMovieInfoById(mid:Int): LiveData<Seats> {
        return seatRepo.getSeatByMid(mid)
    }
    fun getSeatInfoFromNet(mid:Int): Call<Seats> {
        return seatRepo.getSeatFromNet(mid)
    }
    fun Save(seat:Seats):Long{
        return seatRepo.save(seat)
    }
}