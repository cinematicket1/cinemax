package com.et.cinemax.viewModel

import android.app.Application
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.et.cinemax.data.CineDatabase
import com.et.cinemax.data.Movie
import com.et.cinemax.Repo.MovieRepo
import retrofit2.Call

class MovieViewModel(application: Application) : AndroidViewModel(application) , Observable {
    private val callbacks: PropertyChangeRegistry by lazy { PropertyChangeRegistry() }
    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.remove(callback)
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.add(callback)
    }
        private val movieRepo:MovieRepo
        val allMovies:LiveData<List<Movie>>

    init {
        val movieda=CineDatabase.getDatabase(application).movieDao()
        movieRepo=MovieRepo(movieda)
        allMovies=movieRepo.getMovies()
    }
    fun getMovieInfoById(id:Int):LiveData<Movie>{
        return movieRepo.getById(id)
    }
    fun getMovieFromNet(): Call<List<Movie>> {

        return movieRepo.getAllMoviesFromNet()
    }
    fun Save(mov:Movie):Long{
        return movieRepo.Save(mov)
    }
    @Bindable
    var title = MutableLiveData<String>()
    @Bindable
    var genere = MutableLiveData<String>()
    @Bindable
    var amount = MutableLiveData<String>()
    @Bindable
    var language = MutableLiveData<String>()
    @Bindable
    var time = MutableLiveData<String>()
    @Bindable
    var date = MutableLiveData<String>()
    @Bindable
    var imglink = MutableLiveData<String>()
    @Bindable
    var description = MutableLiveData<String>()
}