package com.et.cinemax.viewModel

import android.app.Application
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.et.cinemax.data.CineDatabase
import com.et.cinemax.data.User
import com.et.cinemax.Repo.UserRepo
import com.et.cinemax.data.UserDao
import kotlinx.coroutines.launch

class UserViewModel(application: Application) : AndroidViewModel(application), Observable {
    private val callbacks: PropertyChangeRegistry by lazy { PropertyChangeRegistry() }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.remove(callback)
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.add(callback)
    }
    var userda: UserDao
    var userRepo: UserRepo
    val user: LiveData<User>

    init {
            userda= CineDatabase.getDatabase(application).UserDao()
            userRepo= UserRepo(userda)
            user= userRepo.getUserFromDatabase()
    }

    fun saver(user: User)=viewModelScope.launch {
            userRepo.SaveUserInfo(user)
    }
    fun getUserInfo():LiveData<User>{
       return userRepo.getUserFromDatabase()
    }
    @Bindable
    var userBalance = MutableLiveData<Int>()
    @Bindable
    var userName = MutableLiveData<String>()
    @Bindable
    var userPhone = MutableLiveData<String>()
    @Bindable
    var userPass = MutableLiveData<String>()

}