package com.et.cinemax.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.et.cinemax.data.CineDatabase
import com.et.cinemax.data.Ticket
import com.et.cinemax.Repo.TicketRepo
import kotlinx.coroutines.launch

class TicketViewModel(application: Application) : AndroidViewModel(application) {
    val ticketRepo:TicketRepo
    val mytickets:LiveData<List<Ticket>>
    init {
        val ticketDa= CineDatabase.getDatabase(application).TicketDao()
        ticketRepo= TicketRepo(ticketDa)
        mytickets=ticketRepo.getAllTickets()
    }
    fun Save(ticket: Ticket)=viewModelScope.launch {
        ticketRepo.Save(ticket)
    }
    fun DeleteTest()=viewModelScope.launch {
            ticketRepo.DeleteAll()
        }
    }
