package com.et.cinemax
/*
import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.et.cinemax.databinding.FragmentMovieDetailReseveBinding
import com.et.cinemax.view.LoginFragment
import com.et.cinemax.view.MovieList
import com.et.cinemax.view.movie_detail_reseve
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

@RunWith(AndroidJUnit4::class)
class NavigationTests {

    @Mock
    var mockNavController: NavController = mock(NavController::class.java)
    private lateinit var activity: ActivityTestRule<MainActivity>

    @Before
    fun initRes() {
        mockNavController = mock(NavController::class.java)
        activity = ActivityTestRule(MainActivity::class.java)
    }

     @Test
        fun NavigateFromMovieDetailToMyTicket() {
            launchFragmentInContainer<movie_detail_reseve>(Bundle().apply {
                putInt("MovieID", 5)}, R.style.AppTheme2)
            onView(withId(R.id.my_tickets_btn)).perform(click())
         verify(mockNavController).navigate(R.id.action_movie_detail_reseve_to_myTickets)
     }
    @Test
    fun NavigateFromMovieDetailToMySetting() {
        launchFragmentInContainer<movie_detail_reseve>(Bundle().apply {
            putInt("MovieID", 5)}, R.style.AppTheme2)
        onView(withId(R.id.setting_button)).perform(click())
        verify(mockNavController).navigate(R.id.action_movie_detail_reseve_to_settingsFragment)
    }
    @Test
    fun NavigateFromMovieListToMyTicket() {
        launchFragmentInContainer<MovieList>(Bundle(), R.style.AppTheme2)
        onView(withId(R.id.my_tickets_btn)).perform(click())
        verify(mockNavController).navigate(R.id.action_movieList_to_myTickets)
    }
    @Test
    fun NavigateFromMovieListToMySetting() {
        launchFragmentInContainer<MovieList>(Bundle(), R.style.AppTheme2)
        onView(withId(R.id.setting_button)).perform(click())
        verify(mockNavController).navigate(R.id.action_movieList_to_settingsFragment)
    }
    @Test
    fun NavigateFromLoginToSignUp() {
        launchFragmentInContainer<LoginFragment>(Bundle(), R.style.AppTheme2)
        onView(withId(R.id.link_signup)).perform(click())
        verify(mockNavController).navigate(R.id.action_loginFragment_to_signUpFragment)
    }
}
*/